import { Row, Col, Card, Image, Button } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
export default function Admin(){
		
	return (

		<Row className="mt-4 mb-3">
		<Col xs={12} md={12} className="mt-1 mb-3">
<<<<<<< HEAD
		<h1 className="mt-1 mb-4">Admin's Dashboard</h1>
=======
		<h1 className="mt-1 mb-4">Administrator Dashboard</h1>
>>>>>>> 8d0bec14ec9944660e941782ae9b2c034495ed51
		</Col>
			

			<Col xs={12} md={6} lg={4}>
				<Card className="cardHighlight p-3 bg-light">
				  <Card.Body>
				    <Card.Title>
				    <h3>Product Database</h3>
				    </Card.Title>
				    <Card.Text>
<<<<<<< HEAD
				      Access the database product info
				    </Card.Text>
				    <div className="d-flex justify-content-center">
				   	<Button variant="outline-dark" type="submit" as={NavLink} to="/product-database">View products</Button> 	
=======
				      Access the database for all available and unavailable products
				    </Card.Text>
				    <div className="d-flex justify-content-center">
				   	<Button variant="warning" type="submit" as={NavLink} to="/product-database">View products</Button> 	
>>>>>>> 8d0bec14ec9944660e941782ae9b2c034495ed51
				   	</div>
				  </Card.Body>
				</Card>
			</Col>


			<Col xs={12} md={6} lg={4}>
				<Card className="cardHighlight p-3 bg-light">
				  <Card.Body>
				    <Card.Title>
				    <h3>Add Product</h3>
				    </Card.Title>
				    <Card.Text>
				      Add a new item/product to the database
				    </Card.Text>
				    <div className="d-flex justify-content-center">
<<<<<<< HEAD
				    <Button variant="outline-dark" type="submit" as={NavLink} to="/add-product">Add a product</Button>
=======
				    <Button variant="warning" type="submit" as={NavLink} to="/add-product">Add a product</Button>
>>>>>>> 8d0bec14ec9944660e941782ae9b2c034495ed51
				    </div>
				  </Card.Body>
				</Card>
			</Col>


			<Col xs={12} md={6} lg={4}>
				<Card className="cardHighlight p-3 bg-light">
				  <Card.Body>
				    <Card.Title>
				    <h3>User Database</h3>
				    </Card.Title>
				    <Card.Text>
<<<<<<< HEAD
				      View all registered users including administrators
				    </Card.Text>
				    <div className="d-flex justify-content-center">
				    <Button variant="outline-dark" type="submit" as={NavLink} to="/user-database">View users</Button>
=======
				      View all registered users and administrators in the database
				    </Card.Text>
				    <div className="d-flex justify-content-center">
				    <Button variant="warning" type="submit" as={NavLink} to="/user-database">View users</Button>
>>>>>>> 8d0bec14ec9944660e941782ae9b2c034495ed51
				    </div>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={6} lg={4}>
				<Card className="cardHighlight p-3 bg-light mt-4">
				  <Card.Body>
				    <Card.Title>
				    <h3>Orders Database</h3>
				    </Card.Title>
				    <Card.Text>
				      View all orders made by registered users
				    </Card.Text>
				    <div className="d-flex justify-content-center">
<<<<<<< HEAD
				    <Button variant="outline-dark" type="submit" as={NavLink} to="/order-database">View orders</Button>
=======
				    <Button variant="warning" type="submit" as={NavLink} to="/order-database">View orders</Button>
>>>>>>> 8d0bec14ec9944660e941782ae9b2c034495ed51
				    </div>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}



 
