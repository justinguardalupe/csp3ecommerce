import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button, Row, Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useHistory } from 'react-router-dom';

export default function Register(){
	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobile, setMobile] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	const {user, setUser} = useContext(UserContext);
	let history = useHistory();

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
  e.preventDefault();
<<<<<<< HEAD
  fetch('https://fathomless-forest-22794.herokuapp.com/users/checkEmail',{
=======
  fetch('http://localhost:4000/users/checkEmail',{
>>>>>>> 8d0bec14ec9944660e941782ae9b2c034495ed51
    method: 'POST',
    headers:{
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        email:email
    })
  })
  .then(res => res.json())
  .then(data => {

    if(data === false){
<<<<<<< HEAD
      fetch('https://fathomless-forest-22794.herokuapp.com/users/register',{
=======
      fetch('http://localhost:4000/users/register',{
>>>>>>> 8d0bec14ec9944660e941782ae9b2c034495ed51
        method: 'POST',
        headers:{
            'Content-Type':'application/json'
    },
		body: JSON.stringify({
            firstName:firstName,
            lastName:lastName,
            email:email,
            password:password1,
            mobileNo:mobile
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(`${data}DATA`);
        Swal.fire({
            title:"Registration Successful",
            icon:"success",
            text:"Please login to access the site."
        });
        history.push('/login')
      })
}
    else{
      Swal.fire({
          title:"Duplicate email found",
          icon:"error",
          text:"Please provide a different email."
      })
    }
  })
	setEmail('');
  setPassword2('');
  setPassword1('');
  setFirstName('');
  setLastName('');
  setMobile('');


}

	useEffect(() => {
		// Validate to enable submit button when all fields are populated and both passwords match 
		if((firstName !== '' && lastName !== '' && email !== '' && mobile !== '' && password1 !=='' && password2 !=='') && (password1 === password2))
		{
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobile, password1, password2])



	return (	
		(user.id !== null) ?
		 	<Redirect to="/login" />
		 	:
//		<Form onSubmit={(e) => registerUser(e)}>

	    <Container className="auth-page">
	        <Row className="d-flex align-items-center">
	          <Col md={12} lg={6} className="left-column">
	          	<div className="widget-auth">
	          	<div className="d-flex align-items-center justify-content-between">
	                <p className="auth-header">Sign Up</p>
	            </div>

				<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="firstName">
				  	<Form.Text>First Name</Form.Text>
				  	<Form.Control 
				  	type="firstName" 
				  	placeholder="Enter first name" 
				  	className="input-transparent pl-3"
				  	value={firstName}
				  	onChange = { e => setFirstName(e.target.value) }
				  	required />
				</Form.Group>

				<Form.Group controlId="lastName" className="mt-3">
				  	<Form.Text>Last Name</Form.Text>
				  	<Form.Control 
				  	type="lastName" 
				  	placeholder="Enter last name"
				  	className="input-transparent pl-3" 
				  	value={lastName}
				  	onChange = { e => setLastName(e.target.value) }
				  	required />
				</Form.Group>

			  	<Form.Group controlId="userEmail" className="mt-3">
			    	<Form.Text>Email address</Form.Text>
			    	<Form.Control 
			    	type="email" 
			    	placeholder="Enter email"
			    	className="input-transparent pl-3" 
			    	value={email}
			    	onChange = { e => setEmail(e.target.value) }
			    	required />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>   	
			  	</Form.Group>

			  	<Form.Group controlId="mobile" className="mt-3">
				  	<Form.Text>Mobile Number</Form.Text>
				  	<Form.Control 
				  	type="mobile" 
				  	placeholder="Enter mobile number"
				  	className="input-transparent pl-3" 
				  	value={mobile}
				  	onChange = { e => setMobile(e.target.value) }
				  	required />
				</Form.Group>

			  	<Form.Group controlId="password1" className="mt-3">
			    <Form.Text>Password</Form.Text>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Enter password" 
			    	className="input-transparent pl-3"
			    	value={password1}
			    	onChange = { e => setPassword1(e.target.value) }
			    	required />
			  	</Form.Group>
			  		
			  	<Form.Group controlId="password2" className="mt-3">
			  	<Form.Text>Verify Password</Form.Text>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Re-enter password"
			    	className="input-transparent pl-3" 
			    	value={password2}
			    	onChange = { e => setPassword2(e.target.value) }
			    	required />
			  	</Form.Group>
			  		
			  	{ isActive ? 
			  		<div className="bg-widget d-flex justify-content-center">
			  			<Button className="my-3" variant="outline-success" type="submit" id="submitBtn">
			    			<b>Submit</b>
			  			</Button>
			  		</div>
			  		: 
			  		<div className="bg-widget d-flex justify-content-center">
			  			<Button className="my-3" type="submit" id="submitBtn" variant="outline-dark" disabled>
			    			<b>Submit</b>
			  			</Button>
			  		</div>
				}
				</Form>

				</div>
				</Col>

			</Row>
		</Container>

		)
	}

